module github.com/achimonchi/CircleCi

go 1.16

require github.com/gorilla/mux v1.8.0 // direct

replace github.com/achimonchi/CircleCi v0.1 => bitbucket.org/reyhanjovie/belajar_circle_ci v0.1
